#!/usr/bin/env python3

# ----------------------------------------------------------------------------
#
#   Color Converter
#   Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#
# ----------------------------------------------------------------------------

import argparse, io, os
from PIL import Image, ImageCms

parser = argparse.ArgumentParser()
parser.add_argument("image", \
    nargs="+", \
    help="the image you want to convert")
args = parser.parse_args()
for image in args.image:
    im = Image.open(image)

    transform = ImageCms.buildTransform(\
        io.BytesIO(im.info.get("icc_profile")) if im.info.get("icc_profile") is not None else os.path.join(os.path.dirname(__file__), "resource", "sRGB-v2-magic.icc"), \
        os.path.join(os.path.dirname(__file__), "resource", "sRGB-v2-magic.icc"), \
        "RGB", "RGB", 1)

    ImageCms.applyTransform(im, transform).save(\
        os.path.splitext(image)[0] + " - sRGB v2.1 (relative colorimetric).tiff")
